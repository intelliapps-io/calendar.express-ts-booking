const bookingData = {
  timeZone: "America/New_York",
  bookingDays: [
    {
      date: "2018-08-26T04:00:00.000Z",
      dateYMD: "2018-08-26",
      dateName: "Sun, Aug 26",
      enabled: false
    },
    {
      date: "2018-08-27T04:00:00.000Z",
      dateYMD: "2018-08-27",
      dateName: "Mon, Aug 27",
      enabled: true
    },
    {
      date: "2018-08-28T04:00:00.000Z",
      dateYMD: "2018-08-28",
      dateName: "Tue, Aug 28",
      enabled: true
    },
    {
      date: "2018-08-29T04:00:00.000Z",
      dateYMD: "2018-08-29",
      dateName: "Wed, Aug 29",
      enabled: true
    },
    {
      date: "2018-08-30T04:00:00.000Z",
      dateYMD: "2018-08-30",
      dateName: "Thu, Aug 30",
      enabled: true
    },
    {
      date: "2018-08-31T04:00:00.000Z",
      dateYMD: "2018-08-31",
      dateName: "Fri, Aug 31",
      enabled: true
    },
    {
      date: "2018-09-01T04:00:00.000Z",
      dateYMD: "2018-09-01",
      dateName: "Sat, Sep 1",
      enabled: false
    },
    {
      date: "2018-09-02T04:00:00.000Z",
      dateYMD: "2018-09-02",
      dateName: "Sun, Sep 2",
      enabled: false
    },
    {
      date: "2018-09-03T04:00:00.000Z",
      dateYMD: "2018-09-03",
      dateName: "Mon, Sep 3",
      enabled: true
    },
    {
      date: "2018-09-04T04:00:00.000Z",
      dateYMD: "2018-09-04",
      dateName: "Tue, Sep 4",
      enabled: true
    },
    {
      date: "2018-09-05T04:00:00.000Z",
      dateYMD: "2018-09-05",
      dateName: "Wed, Sep 5",
      enabled: true
    },
    {
      date: "2018-09-06T04:00:00.000Z",
      dateYMD: "2018-09-06",
      dateName: "Thu, Sep 6",
      enabled: true
    },
    {
      date: "2018-09-07T04:00:00.000Z",
      dateYMD: "2018-09-07",
      dateName: "Fri, Sep 7",
      enabled: true
    },
    {
      date: "2018-09-08T04:00:00.000Z",
      dateYMD: "2018-09-08",
      dateName: "Sat, Sep 8",
      enabled: false
    },
    {
      date: "2018-09-09T04:00:00.000Z",
      dateYMD: "2018-09-09",
      dateName: "Sun, Sep 9",
      enabled: false
    },
    {
      date: "2018-09-10T04:00:00.000Z",
      dateYMD: "2018-09-10",
      dateName: "Mon, Sep 10",
      enabled: true
    },
    {
      date: "2018-09-11T04:00:00.000Z",
      dateYMD: "2018-09-11",
      dateName: "Tue, Sep 11",
      enabled: true
    },
    {
      date: "2018-09-12T04:00:00.000Z",
      dateYMD: "2018-09-12",
      dateName: "Wed, Sep 12",
      enabled: true
    },
    {
      date: "2018-09-13T04:00:00.000Z",
      dateYMD: "2018-09-13",
      dateName: "Thu, Sep 13",
      enabled: true
    },
    {
      date: "2018-09-14T04:00:00.000Z",
      dateYMD: "2018-09-14",
      dateName: "Fri, Sep 14",
      enabled: true
    },
    {
      date: "2018-09-15T04:00:00.000Z",
      dateYMD: "2018-09-15",
      dateName: "Sat, Sep 15",
      enabled: false
    },
    {
      date: "2018-09-16T04:00:00.000Z",
      dateYMD: "2018-09-16",
      dateName: "Sun, Sep 16",
      enabled: false
    },
    {
      date: "2018-09-17T04:00:00.000Z",
      dateYMD: "2018-09-17",
      dateName: "Mon, Sep 17",
      enabled: true
    },
    {
      date: "2018-09-18T04:00:00.000Z",
      dateYMD: "2018-09-18",
      dateName: "Tue, Sep 18",
      enabled: true
    },
    {
      date: "2018-09-19T04:00:00.000Z",
      dateYMD: "2018-09-19",
      dateName: "Wed, Sep 19",
      enabled: true
    },
    {
      date: "2018-09-20T04:00:00.000Z",
      dateYMD: "2018-09-20",
      dateName: "Thu, Sep 20",
      enabled: true
    },
    {
      date: "2018-09-21T04:00:00.000Z",
      dateYMD: "2018-09-21",
      dateName: "Fri, Sep 21",
      enabled: true
    },
    {
      date: "2018-09-22T04:00:00.000Z",
      dateYMD: "2018-09-22",
      dateName: "Sat, Sep 22",
      enabled: false
    },
    {
      date: "2018-09-23T04:00:00.000Z",
      dateYMD: "2018-09-23",
      dateName: "Sun, Sep 23",
      enabled: false
    },
    {
      date: "2018-09-24T04:00:00.000Z",
      dateYMD: "2018-09-24",
      dateName: "Mon, Sep 24",
      enabled: true
    },
    {
      date: "2018-09-25T04:00:00.000Z",
      dateYMD: "2018-09-25",
      dateName: "Tue, Sep 25",
      enabled: true
    },
    {
      date: "2018-09-26T04:00:00.000Z",
      dateYMD: "2018-09-26",
      dateName: "Wed, Sep 26",
      enabled: true
    },
    {
      date: "2018-09-27T04:00:00.000Z",
      dateYMD: "2018-09-27",
      dateName: "Thu, Sep 27",
      enabled: true
    },
    {
      date: "2018-09-28T04:00:00.000Z",
      dateYMD: "2018-09-28",
      dateName: "Fri, Sep 28",
      enabled: true
    },
    {
      date: "2018-09-29T04:00:00.000Z",
      dateYMD: "2018-09-29",
      dateName: "Sat, Sep 29",
      enabled: false
    },
    {
      date: "2018-09-30T04:00:00.000Z",
      dateYMD: "2018-09-30",
      dateName: "Sun, Sep 30",
      enabled: false
    },
    {
      date: "2018-10-01T04:00:00.000Z",
      dateYMD: "2018-10-01",
      dateName: "Mon, Oct 1",
      enabled: true
    },
    {
      date: "2018-10-02T04:00:00.000Z",
      dateYMD: "2018-10-02",
      dateName: "Tue, Oct 2",
      enabled: true
    },
    {
      date: "2018-10-03T04:00:00.000Z",
      dateYMD: "2018-10-03",
      dateName: "Wed, Oct 3",
      enabled: true
    },
    {
      date: "2018-10-04T04:00:00.000Z",
      dateYMD: "2018-10-04",
      dateName: "Thu, Oct 4",
      enabled: true
    },
    {
      date: "2018-10-05T04:00:00.000Z",
      dateYMD: "2018-10-05",
      dateName: "Fri, Oct 5",
      enabled: true
    },
    {
      date: "2018-10-06T04:00:00.000Z",
      dateYMD: "2018-10-06",
      dateName: "Sat, Oct 6",
      enabled: false
    },
    {
      date: "2018-10-07T04:00:00.000Z",
      dateYMD: "2018-10-07",
      dateName: "Sun, Oct 7",
      enabled: false
    },
    {
      date: "2018-10-08T04:00:00.000Z",
      dateYMD: "2018-10-08",
      dateName: "Mon, Oct 8",
      enabled: true
    },
    {
      date: "2018-10-09T04:00:00.000Z",
      dateYMD: "2018-10-09",
      dateName: "Tue, Oct 9",
      enabled: true
    },
    {
      date: "2018-10-10T04:00:00.000Z",
      dateYMD: "2018-10-10",
      dateName: "Wed, Oct 10",
      enabled: true
    },
    {
      date: "2018-10-11T04:00:00.000Z",
      dateYMD: "2018-10-11",
      dateName: "Thu, Oct 11",
      enabled: true
    },
    {
      date: "2018-10-12T04:00:00.000Z",
      dateYMD: "2018-10-12",
      dateName: "Fri, Oct 12",
      enabled: true
    },
    {
      date: "2018-10-13T04:00:00.000Z",
      dateYMD: "2018-10-13",
      dateName: "Sat, Oct 13",
      enabled: false
    },
    {
      date: "2018-10-14T04:00:00.000Z",
      dateYMD: "2018-10-14",
      dateName: "Sun, Oct 14",
      enabled: false
    },
    {
      date: "2018-10-15T04:00:00.000Z",
      dateYMD: "2018-10-15",
      dateName: "Mon, Oct 15",
      enabled: true
    },
    {
      date: "2018-10-16T04:00:00.000Z",
      dateYMD: "2018-10-16",
      dateName: "Tue, Oct 16",
      enabled: true
    },
    {
      date: "2018-10-17T04:00:00.000Z",
      dateYMD: "2018-10-17",
      dateName: "Wed, Oct 17",
      enabled: true
    },
    {
      date: "2018-10-18T04:00:00.000Z",
      dateYMD: "2018-10-18",
      dateName: "Thu, Oct 18",
      enabled: true
    },
    {
      date: "2018-10-19T04:00:00.000Z",
      dateYMD: "2018-10-19",
      dateName: "Fri, Oct 19",
      enabled: true
    },
    {
      date: "2018-10-20T04:00:00.000Z",
      dateYMD: "2018-10-20",
      dateName: "Sat, Oct 20",
      enabled: false
    },
    {
      date: "2018-10-21T04:00:00.000Z",
      dateYMD: "2018-10-21",
      dateName: "Sun, Oct 21",
      enabled: false
    },
    {
      date: "2018-10-22T04:00:00.000Z",
      dateYMD: "2018-10-22",
      dateName: "Mon, Oct 22",
      enabled: true
    },
    {
      date: "2018-10-23T04:00:00.000Z",
      dateYMD: "2018-10-23",
      dateName: "Tue, Oct 23",
      enabled: true
    },
    {
      date: "2018-10-24T04:00:00.000Z",
      dateYMD: "2018-10-24",
      dateName: "Wed, Oct 24",
      enabled: true
    },
    {
      date: "2018-10-25T04:00:00.000Z",
      dateYMD: "2018-10-25",
      dateName: "Thu, Oct 25",
      enabled: true
    },
    {
      date: "2018-10-26T04:00:00.000Z",
      dateYMD: "2018-10-26",
      dateName: "Fri, Oct 26",
      enabled: true
    },
    {
      date: "2018-10-27T04:00:00.000Z",
      dateYMD: "2018-10-27",
      dateName: "Sat, Oct 27",
      enabled: false
    },
    {
      date: "2018-10-28T04:00:00.000Z",
      dateYMD: "2018-10-28",
      dateName: "Sun, Oct 28",
      enabled: false
    },
    {
      date: "2018-10-29T04:00:00.000Z",
      dateYMD: "2018-10-29",
      dateName: "Mon, Oct 29",
      enabled: true
    },
    {
      date: "2018-10-30T04:00:00.000Z",
      dateYMD: "2018-10-30",
      dateName: "Tue, Oct 30",
      enabled: true
    },
    {
      date: "2018-10-31T04:00:00.000Z",
      dateYMD: "2018-10-31",
      dateName: "Wed, Oct 31",
      enabled: true
    },
    {
      date: "2018-11-01T04:00:00.000Z",
      dateYMD: "2018-11-01",
      dateName: "Thu, Nov 1",
      enabled: true
    },
    {
      date: "2018-11-02T04:00:00.000Z",
      dateYMD: "2018-11-02",
      dateName: "Fri, Nov 2",
      enabled: true
    },
    {
      date: "2018-11-03T04:00:00.000Z",
      dateYMD: "2018-11-03",
      dateName: "Sat, Nov 3",
      enabled: false
    },
    {
      date: "2018-11-04T04:00:00.000Z",
      dateYMD: "2018-11-04",
      dateName: "Sun, Nov 4",
      enabled: false
    },
    {
      date: "2018-11-05T05:00:00.000Z",
      dateYMD: "2018-11-05",
      dateName: "Mon, Nov 5",
      enabled: true
    },
    {
      date: "2018-11-06T05:00:00.000Z",
      dateYMD: "2018-11-06",
      dateName: "Tue, Nov 6",
      enabled: true
    },
    {
      date: "2018-11-07T05:00:00.000Z",
      dateYMD: "2018-11-07",
      dateName: "Wed, Nov 7",
      enabled: true
    },
    {
      date: "2018-11-08T05:00:00.000Z",
      dateYMD: "2018-11-08",
      dateName: "Thu, Nov 8",
      enabled: true
    },
    {
      date: "2018-11-09T05:00:00.000Z",
      dateYMD: "2018-11-09",
      dateName: "Fri, Nov 9",
      enabled: true
    },
    {
      date: "2018-11-10T05:00:00.000Z",
      dateYMD: "2018-11-10",
      dateName: "Sat, Nov 10",
      enabled: false
    },
    {
      date: "2018-11-11T05:00:00.000Z",
      dateYMD: "2018-11-11",
      dateName: "Sun, Nov 11",
      enabled: false
    },
    {
      date: "2018-11-12T05:00:00.000Z",
      dateYMD: "2018-11-12",
      dateName: "Mon, Nov 12",
      enabled: true
    },
    {
      date: "2018-11-13T05:00:00.000Z",
      dateYMD: "2018-11-13",
      dateName: "Tue, Nov 13",
      enabled: true
    },
    {
      date: "2018-11-14T05:00:00.000Z",
      dateYMD: "2018-11-14",
      dateName: "Wed, Nov 14",
      enabled: true
    },
    {
      date: "2018-11-15T05:00:00.000Z",
      dateYMD: "2018-11-15",
      dateName: "Thu, Nov 15",
      enabled: true
    },
    {
      date: "2018-11-16T05:00:00.000Z",
      dateYMD: "2018-11-16",
      dateName: "Fri, Nov 16",
      enabled: true
    },
    {
      date: "2018-11-17T05:00:00.000Z",
      dateYMD: "2018-11-17",
      dateName: "Sat, Nov 17",
      enabled: false
    },
    {
      date: "2018-11-18T05:00:00.000Z",
      dateYMD: "2018-11-18",
      dateName: "Sun, Nov 18",
      enabled: false
    },
    {
      date: "2018-11-19T05:00:00.000Z",
      dateYMD: "2018-11-19",
      dateName: "Mon, Nov 19",
      enabled: true
    },
    {
      date: "2018-11-20T05:00:00.000Z",
      dateYMD: "2018-11-20",
      dateName: "Tue, Nov 20",
      enabled: true
    },
    {
      date: "2018-11-21T05:00:00.000Z",
      dateYMD: "2018-11-21",
      dateName: "Wed, Nov 21",
      enabled: true
    },
    {
      date: "2018-11-22T05:00:00.000Z",
      dateYMD: "2018-11-22",
      dateName: "Thu, Nov 22",
      enabled: true
    },
    {
      date: "2018-11-23T05:00:00.000Z",
      dateYMD: "2018-11-23",
      dateName: "Fri, Nov 23",
      enabled: true
    },
    {
      date: "2018-11-24T05:00:00.000Z",
      dateYMD: "2018-11-24",
      dateName: "Sat, Nov 24",
      enabled: false
    },
    {
      date: "2018-11-25T05:00:00.000Z",
      dateYMD: "2018-11-25",
      dateName: "Sun, Nov 25",
      enabled: false
    },
    {
      date: "2018-11-26T05:00:00.000Z",
      dateYMD: "2018-11-26",
      dateName: "Mon, Nov 26",
      enabled: true
    },
    {
      date: "2018-11-27T05:00:00.000Z",
      dateYMD: "2018-11-27",
      dateName: "Tue, Nov 27",
      enabled: true
    },
    {
      date: "2018-11-28T05:00:00.000Z",
      dateYMD: "2018-11-28",
      dateName: "Wed, Nov 28",
      enabled: true
    },
    {
      date: "2018-11-29T05:00:00.000Z",
      dateYMD: "2018-11-29",
      dateName: "Thu, Nov 29",
      enabled: true
    },
    {
      date: "2018-11-30T05:00:00.000Z",
      dateYMD: "2018-11-30",
      dateName: "Fri, Nov 30",
      enabled: true
    },
    {
      date: "2018-12-01T05:00:00.000Z",
      dateYMD: "2018-12-01",
      dateName: "Sat, Dec 1",
      enabled: false
    },
    {
      date: "2018-12-02T05:00:00.000Z",
      dateYMD: "2018-12-02",
      dateName: "Sun, Dec 2",
      enabled: false
    },
    {
      date: "2018-12-03T05:00:00.000Z",
      dateYMD: "2018-12-03",
      dateName: "Mon, Dec 3",
      enabled: true
    },
    {
      date: "2018-12-04T05:00:00.000Z",
      dateYMD: "2018-12-04",
      dateName: "Tue, Dec 4",
      enabled: true
    },
    {
      date: "2018-12-05T05:00:00.000Z",
      dateYMD: "2018-12-05",
      dateName: "Wed, Dec 5",
      enabled: true
    },
    {
      date: "2018-12-06T05:00:00.000Z",
      dateYMD: "2018-12-06",
      dateName: "Thu, Dec 6",
      enabled: true
    },
    {
      date: "2018-12-07T05:00:00.000Z",
      dateYMD: "2018-12-07",
      dateName: "Fri, Dec 7",
      enabled: true
    },
    {
      date: "2018-12-08T05:00:00.000Z",
      dateYMD: "2018-12-08",
      dateName: "Sat, Dec 8",
      enabled: false
    },
    {
      date: "2018-12-09T05:00:00.000Z",
      dateYMD: "2018-12-09",
      dateName: "Sun, Dec 9",
      enabled: false
    },
    {
      date: "2018-12-10T05:00:00.000Z",
      dateYMD: "2018-12-10",
      dateName: "Mon, Dec 10",
      enabled: true
    },
    {
      date: "2018-12-11T05:00:00.000Z",
      dateYMD: "2018-12-11",
      dateName: "Tue, Dec 11",
      enabled: true
    },
    {
      date: "2018-12-12T05:00:00.000Z",
      dateYMD: "2018-12-12",
      dateName: "Wed, Dec 12",
      enabled: true
    },
    {
      date: "2018-12-13T05:00:00.000Z",
      dateYMD: "2018-12-13",
      dateName: "Thu, Dec 13",
      enabled: true
    },
    {
      date: "2018-12-14T05:00:00.000Z",
      dateYMD: "2018-12-14",
      dateName: "Fri, Dec 14",
      enabled: true
    },
    {
      date: "2018-12-15T05:00:00.000Z",
      dateYMD: "2018-12-15",
      dateName: "Sat, Dec 15",
      enabled: false
    }
  ],
  timeSlots: [
    {
      start: 540,
      end: 570
    },
    {
      start: 570,
      end: 600
    },
    {
      start: 600,
      end: 630
    },
    {
      start: 630,
      end: 660
    },
    {
      start: 660,
      end: 690
    },
    {
      start: 690,
      end: 720
    },
    {
      start: 720,
      end: 750
    },
    {
      start: 750,
      end: 780
    },
    {
      start: 780,
      end: 810
    },
    {
      start: 810,
      end: 840
    },
    {
      start: 840,
      end: 870
    },
    {
      start: 870,
      end: 900
    },
    {
      start: 900,
      end: 930
    },
    {
      start: 930,
      end: 960
    },
    {
      start: 960,
      end: 990
    },
    {
      start: 990,
      end: 1020
    }
  ]
};
export default bookingData;
