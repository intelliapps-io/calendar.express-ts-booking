// Type definitions for [~THE LIBRARY NAME~] [~OPTIONAL VERSION NUMBER~]
// Project: [~THE PROJECT NAME~]
// Definitions by: [~YOUR NAME~] <[~A URL FOR YOU~]>

import * as moment from "moment";

declare global {
  interface IBookingDay {
    date: string;
    dateYMD: string;
    dateName: string;
    enabled: boolean;
    existingEvents?: Array<string>;
  }

  interface IBookingData {
    timeZone: string;
    bookingDays: Array<IBookingDay>;
    timeSlots: Array<ITimeSlot>;
  }

  interface ITimeSlot {
    start: number;
    end: number;
  }

  interface IMomentTimeSlot {
    start: moment.Moment;
    end: moment.Moment;
    startMinuets: number;
    endMinuets: number;
  }

  interface IState {
    // Observables
    bookingDays: Array<IBookingDay>;
    timeSlots: Array<ITimeSlot>;
    serverTimeZone: string;
    attendeeTimeZone: string;
    activeBookingDayIndex: number;

    // Computed
    activeBookingDay: IBookingDay;
    momentTimeSlots: Array<IMomentTimeSlot>;
    momentToAttendeTimeZone: Function

    // Actions
    eventDuration: number;
  }
}

/*~ If your module exports nothing, you'll need this line. Otherwise, delete it */
export {};
