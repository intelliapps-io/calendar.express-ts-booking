import { observable, computed, action } from "mobx";
import * as moment from "moment";
import "moment-timezone";

export interface IDateYMD {
    year: number,
    month: number,
    day: number
}

class AppState {
    constructor(bookingDays: Array<IBookingDay>, timeSlots: Array<ITimeSlot>, serverTimeZone: string) {
        this.bookingDays = bookingDays; this.timeSlots = timeSlots; this.serverTimeZone = serverTimeZone;
    }
    
    @observable bookingDays: Array<IBookingDay>;    // Constant, Booking Data
    @observable timeSlots: Array<ITimeSlot>;        // Constant, Booking Data
    @observable serverTimeZone: string;             // Constant, Booking Data

    @observable attendeeTimeZone: string = moment.tz.guess();
    @observable activeBookingDayIndex: number = 0;

    /////////////
    // Computed
    /////////////

    @computed get activeBookingDay() { 
        const { date, dateName, dateYMD, enabled, existingEvents } = this.bookingDays[this.activeBookingDayIndex];
        const returnValue: IBookingDay = { date, dateName, dateYMD, enabled, existingEvents };
        return returnValue; 
    };

    @computed get momentTimeSlots() {
        const { dateYMD } = this.bookingDays[this.activeBookingDayIndex];
        let timeSlotDateTimes: Array<IMomentTimeSlot> = [];
        this.timeSlots.map(timeSlot => {
            timeSlotDateTimes.push({
                start:  moment(dateYMD, ["YYYY-MM-DD"]).tz(this.serverTimeZone).add(timeSlot.start, "minutes"),
                end: moment(dateYMD, ["YYYY-MM-DD"]).tz(this.serverTimeZone).add(timeSlot.end, "minutes"),
                startMinuets: timeSlot.start,
                endMinuets: timeSlot.end
            });
        });
        return timeSlotDateTimes;
    }

    @computed get eventDuration() {
        return this.timeSlots[0].end - this.timeSlots[0].start;
    }

    ////////////
    // Actions
    ////////////

    @action setAttendeeTimeZone(timeZone: string) {
        this.attendeeTimeZone = timeZone;
    }

    @action setActiveBookingDay(index: number) {
        this.activeBookingDayIndex = index;
    }

    ////////////
    // Helpers
    ////////////
    stringToIDateYMD(str: string): IDateYMD {
        try {
            let dateYMD: IDateYMD;
            dateYMD = {
                year: parseInt(str.slice(0, 4)),
                month: parseInt(str.slice(5, 7)),
                day: parseInt(str.slice(8, 10))
            }
            return dateYMD;
        } catch(e) {
            throw new Error(e);
        }
    }

    stringToDate(str: string): Date {
        const { year, month, day } = this.stringToIDateYMD(str);
        return new Date(year, month - 1, day);
    }

    momentToAttendeTimeZone(mmt: moment.Moment) {
        return moment(mmt).tz(this.attendeeTimeZone);
    }
}

export default AppState;
