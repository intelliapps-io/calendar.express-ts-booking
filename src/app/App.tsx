import * as React from "react";
import { inject, observer } from "mobx-react";
import DevTools from "mobx-react-devtools";
import { Layout, Row, Col, Divider } from "antd";
const { Content } = Layout;

import "antd/dist/antd.css";
import "./app.style.scss";
import WeekControls from "./components/WeekControls/WeekControls";
import TimeSlots from "./components/TimeSlots/TimeSlots";
import TimeZoneSelect from "./components/TimeZoneSelect/TimeZoneSelect";

@inject("appState")
@observer
class App extends React.Component<{ appState?: IState }> {
  public render() {
    const { eventDuration, attendeeTimeZone } = this.props.appState;
    return (
      <Layout className="layout">
        <DevTools />
        <Content className="content" style={{ padding: "50px" }}>  
            <Row className="header">
              <Col span={12}>
                <h3>Duration {eventDuration} Minuets</h3>
              </Col>
              <Col span={12}>
                <TimeZoneSelect />
              </Col>
            </Row>        
            <div className="container">
            <Row>
              <Col span={12}>
                <WeekControls />
                <TimeSlots />
              </Col>
              <Col span={12}>
                <p>Event Description</p>
              </Col>
            </Row>
          </div>
        </Content>
      </Layout>
    );
  }
}

export default App;
