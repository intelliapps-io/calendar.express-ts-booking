import * as React from "react";
import { inject, observer } from "mobx-react";

export interface IProps {
  appState?: IState
}

@inject("appState") @observer 
class WeekControls extends React.Component<IProps> {
  render() {
    // console.log(this.props.appState);
    return(
      <div>
        <h2>WeekControls</h2>
      </div>
    );
  };
}

export default WeekControls;