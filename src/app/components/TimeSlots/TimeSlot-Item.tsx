import * as React from "react";
import * as moment from "moment";
import { Icon } from "antd";

import "./TimeSlots.style.scss";

export interface IProps {
  momentTimeSlot: IMomentTimeSlot;
  attendeeTimeZone: string;
}

class TimeSlotItem extends React.Component<IProps> {
  render() {
    const { attendeeTimeZone } = this.props;
    const start = moment(this.props.momentTimeSlot.start).tz(attendeeTimeZone);
    // const end = moment(this.props.momentTimeSlot.end).tz(attendeeTimeZone);
    return(
      <div className="timeslot-item">
        <span className={`hour-minutets`}>{moment(start).format("h:mm")}</span>
        <span>{moment(start).format("a")}</span>
        <span className="icon-btn" style={{ float: "right" }}><Icon type="right"/></span>
      </div>
    );
  };
}

export default TimeSlotItem;