import * as React from "react";
import { inject, observer } from "mobx-react";

import TimeSlotItem from "./TimeSlot-Item";

export interface IProps {
  appState?: IState
  className?: string
}

@inject("appState") @observer 
class TimeSlots extends React.Component<IProps> {

  renderTimeSlots( { momentTimeSlots, attendeeTimeZone }:{ momentTimeSlots: Array<IMomentTimeSlot>, attendeeTimeZone: string}) {
    return momentTimeSlots.map((timeSlot: IMomentTimeSlot, i) => {
      return <TimeSlotItem key={i} momentTimeSlot={timeSlot} attendeeTimeZone={attendeeTimeZone}/>
    });
  }

  render() {
    const { momentTimeSlots, attendeeTimeZone } = this.props.appState;
    return(
      <div className={`timeslots-wrapper ${this.props.className}`}>
        {this.renderTimeSlots({momentTimeSlots, attendeeTimeZone})}
      </div>
    );
  };
}

export default TimeSlots;