import * as React from "react";
import * as moment from "moment";
import "moment-timezone";

export interface IProps {
  appState?: IState
}

class TimeZoneSelect extends React.Component<IProps> {
  render() {
    const timeZones: string[] = moment.tz.names();
    console.log(timeZones);
    return(
      <div>
        <h2>TimeZoneSelect</h2>
      </div>
    );
  };
}

export default TimeZoneSelect;